(function() {
    'use strict';

    angular
        .module('sweeh.main')
        .config(config)

    config.$inject = ['$stateProvider', '$urlRouterProvider'];    

    function config($stateProvider, $urlRouterProvider) {

        var state = {
            name: 'main',
            url: '/main',
            templateUrl: 'app/main/main.html',
            controller: 'MainCtrl as vm',            
        };
                
        $stateProvider.state(state);
    }
})();