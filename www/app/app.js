(function() {
    'use strict';

    angular
        .module('sweeh', [
            //3rd party modules
            'ionic',
            'ngMaterial',
            'ngCordova',
            'ngOpenFB',
            //App modules
            'sweeh.login',
            'sweeh.chats',
            'sweeh.stage',
            'sweeh.main',
            ])
        .config(config)
        .run(run);

        function config($urlRouterProvider, $mdIconProvider, $cordovaFacebookProvider){
            //Return to login if matches /
            $urlRouterProvider.when('/', '/login');

            //Default state for redirect        
            $urlRouterProvider.otherwise('/login');

            $mdIconProvider
              .iconSet('toggle', 'icons/toggle-icons.svg', 24)
              .iconSet('action', 'icons/action-icons.svg', 24)
              .iconSet('social', 'icons/social-icons.svg', 24)
              .defaultIconSet('icons/social-icons.svg', 24);

            //@TOREMOVE only for development!!
            //var appID = 1421222791517262;
            //var version = "v2.3"; // or leave blank and default is v2.0
            //$cordovaFacebookProvider.browserInit(appID, version);            
            //console.log($cordovaFacebookProvider);
        }

        function run($rootScope, $ionicPlatform, ngFB){
            //For displaying more in-deph ui-router errors    
            console.log('Running app phase');
            $rootScope.$on("$stateChangeError", console.log.bind(console));    
            ngFB.init({appId: '1421222791517262'});
        }
})();