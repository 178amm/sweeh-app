﻿(function () {
    'use strict';
    
    angular
        .module('sweeh.stage')
        .controller('StageCtrl', stage);
    
    stage.$inject = ['$state'];
    
    function stage($state){
    	var vm = this;

    	//Vars
    	vm.$state = $state;
    	vm.change = function(){
    		console.log('func');
    	}

    }
    
})();