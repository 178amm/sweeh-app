(function() {
    'use strict';

    angular
        .module('sweeh.stage')
        .config(config)

    config.$inject = ['$stateProvider', '$urlRouterProvider'];    

    function config($stateProvider, $urlRouterProvider) {

        var state = {
            name: 'main.stage',
            url: '/stage',
            views: {
              'stage': {
                templateUrl: 'app/stage/stage.html',
                controller: 'StageCtrl as vm',
              }
            }            
        };
                
        $stateProvider.state(state);
    }
})();