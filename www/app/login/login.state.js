(function() {
    'use strict';

    angular
        .module('sweeh.login')
        .config(config)

    config.$inject = ['$stateProvider', '$urlRouterProvider'];    

    function config($stateProvider, $urlRouterProvider) {

        var state = {
            name: 'login',
            url: '/login',
            templateUrl: 'app/login/login.html',
            controller: 'LoginCtrl as vm',            
        };
                
        $stateProvider.state(state);
    }
})();