﻿(function () {
    'use strict';
    
    angular
        .module('sweeh.login')
        .controller('LoginCtrl', Login);
    
    Login.$inject = ['$state', 'ngFB'];
    
    function Login($state, ngFB){
    	var vm = this;

    	//Vars

        //Protos
        vm.login = login;
        
        activate();
        ////////////////////////

        //Funcs
        function activate(){

        }

        function login(){
            ngFB.login({scope: ''}).then(
                    function(response) {
                        alert('Facebook login succeeded, got access token: ' + response.authResponse.accessToken);
                    },
                    function(error) {
                        alert('Facebook login failed: ' + error);
                    });
        }

        

    }
    
})();