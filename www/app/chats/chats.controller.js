﻿(function () {
    'use strict';
    
    angular
        .module('sweeh.chats')
        .controller('ChatsCtrl', chats);
    
    chats.$inject = ['$state'];
    
    function chats($state){
    	var vm = this;

    	//Vars
    	vm.$state = $state;
    	vm.change = function(){
    		console.log('func');
    	}

    }
    
})();