(function() {
    'use strict';

    angular
        .module('sweeh.chats')
        .config(config)

    config.$inject = ['$stateProvider', '$urlRouterProvider'];    

    function config($stateProvider, $urlRouterProvider) {

        var state = {
            name: 'main.chats',
            url: '/chats',
            views: {
              'chats': {
                templateUrl: 'app/chats/chats.html',
                controller: 'ChatsCtrl as vm',
              }
            }            
        };
                
        $stateProvider.state(state);
    }
})();